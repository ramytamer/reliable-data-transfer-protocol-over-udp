from random import *

all_transmittions = 0     #number of transmitted packets
packets_should_be_sent = 8   #number of packets
lossProbability = randint(1,9) / 10.0
packets_should_be_lost = int(packets_should_be_sent * lossProbability)
print(packets_should_be_lost)


def should_i_send():
	global packets_should_be_sent
	global packets_should_be_lost


	if packets_should_be_sent:
		toSentOrNotToSent = randint(0,1)
		if(packets_should_be_lost == 0):
			#send all directly
			packets_should_be_sent -= 1
			return True
	 	elif(packets_should_be_sent == packets_should_be_lost):
	 		#all remaining packets will be lost and retransmitted
	 		packets_should_be_sent -= 1 
	 		packets_should_be_lost -= 1
	 		return False
	 	else:
			if(toSentOrNotToSent == 1):
				#send
				packets_should_be_sent-=1
				return True

			else:
				#packet is lost :O
				packets_should_be_lost -= 1
				return False

#all_transmittions = packets_should_be_sent + packets_should_be_lost
print(all_transmittions)