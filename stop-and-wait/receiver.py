import socket
import sys
import signal
import packet
import os
import time
from threading import Timer
import timeit

start = timeit.default_timer()

os.system('clear')

__HOST = sys.argv[1]
__PORT = int(sys.argv[2])
CHUNK_SIZE = 512
INP_FILE    = str(sys.argv[3]) if len(sys.argv) >= 4 else 'input.txt'
OUT_FILE    = str(sys.argv[4]) if len(sys.argv) >= 5 else 'output'

server_address = (__HOST, __PORT)
connection = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# connection.bind(('localhost', 0)) 

print 'connecting...'
# connection.settimeout(2)

def close_server(signal, frame):
	print "\b\bClosing ..."
	connection.close()
	exit(0)

def handle_timeout(header_dict):
	# print 'TIMEOUT'
	pckts_timer.cancel()
	# print 'resending'
	connection.sendto(last_pkt, (__HOST, header_dict['src_port']))

	header, data = packet.unpack(last_pkt, False)
	packet.details(header_dict, data, 'RTRS', True)

	global pkts_re_sent
	pkts_re_sent += 1

	# print 'restart timer'
	global pckts_timer
	pckts_timer = Timer(0.5, handle_timeout, (header_dict, ))
	pckts_timer.start()

signal.signal(signal.SIGINT, close_server)

# Handshake with server
src_port = connection.getsockname()[1]
seq_num = 0
flags = {
	'SYN': True,
	'FIN': False,
	'ACK': False
}

msg = packet.create(src_port, __PORT, seq_num, flags, INP_FILE)

# BOOTSTRAP OUTPUT FILE AREA
if os.path.isfile(OUT_FILE) :
	os.remove(OUT_FILE)
	# Empty the file
	# open(OUT_FILE, 'w').close()

# Open file in append mode
output_file = open(OUT_FILE, 'a')


# print 'raw request: \n', msg

last_seq_num = 0
pkts_re_sent = 0
last_pkt     = None
pckts_timer  = None

to_drop      = False

try:
	# print server_address
	connection.sendto(msg, server_address)

	while True:
		msg, address = connection.recvfrom( packet.HEADER_LENGTH + CHUNK_SIZE ) # 19 HL + 500 byte from file

		if pckts_timer != None:
			# print 'stopping timer'
			pckts_timer.cancel()

		header, data = packet.unpack(msg, False)
		header_dict  = packet.dictionarify(header)

		additional_data = ''


		# Check if packet is valid
		if not packet.isValid(msg): 
			# Handle error in checksum (Send negative ack)
			flags = packet.set_flags(ACK = False)
			nack_packet = packet.create(src_port, __PORT, header_dict['seq_num'], flags, 'Corrupted Packet') # no need to send data again
			connection.sendto(nack_packet, server_address)

			last_pkt = nack_packet
			pckts_timer = Timer(0.5, handle_timeout, (header_dict, ))
			pckts_timer.start()

			continue
		
		else:
			packet.details(header, data, 'RECV')

		
		if header_dict['SYN'] and header_dict['ACK']:
			continue

		# In case server is sending same/old packet again
		# 
		# NOTE: IT COULD BE REFACOTRED BUT JUST KEEP IT LIKE THAT FOR SIMPLICITY
		if last_seq_num >= header_dict['seq_num']:
			# Send last ACK again
			# with last seq num we sent before
			# new_pckt = packet.create(src_port, __PORT, last_seq_num, packet.set_flags())
			x = 1 # do noting 
		else:
			last_seq_num = header_dict['seq_num']

			if data != 'EOP':
				output_file.write(data)
			# Send ACK to server
			# new_pckt = packet.create(src_port, __PORT, last_seq_num, packet.set_flags())

		

		# If Last Packet
		if header_dict['FIN'] == True:
			if header_dict['ACK']:
				continue
			else:
				# Is the last packet
				# Close file handler
				output_file.close()
				# Send ACK=1 with FIN=1 to server
				# new_pckt = packet.create(src_port, __PORT, last_seq_num, packet.set_flags(FIN = True)) # No data need to be sent
				flags = packet.set_flags(FIN = True, ACK = True)
				# connection.sendto(new_pckt, server_address)
				# settimeout if ended then will raise exception (close server)
				connection.settimeout(3)
				# continue
		else:
			if data == 'EOP':
				# nshof lw el client 3iz 7ga tani wla y2fl
				flags = packet.set_flags(FIN=True, ACK = False)
				last_seq_num += 1
			else:
				flags = packet.set_flags()

		

		new_pckt = packet.create(src_port, __PORT, last_seq_num, flags, additional_data) # No data need to be sent
		connection.sendto(new_pckt, server_address)

		last_pkt = new_pckt

		# If FIN packet then no timer need to be setuped
		if not header_dict['FIN']:
			pckts_timer = Timer(0.5, handle_timeout, (header_dict, ))
			pckts_timer.start()

	# print 'raw response: \n', data

except socket.timeout:
	print 'closing...'
	connection.close()

connection.close()


stop = timeit.default_timer()

print 'Elapsed Time: ', stop - start 