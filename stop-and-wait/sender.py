import socket
import sys
import signal
import packet
import os
from threading import Timer
import time
import math
from random import *

# seed(time.time())


os.system('clear')

__HOST			= sys.argv[1]
__PORT			= int(sys.argv[2])
lossProbability	= (int(sys.argv[3]) / 100.0) if len(sys.argv) >= 4 else randint(0, 10) / 10.0
# lossProbability	= (int(sys.argv[3]) / 100.0) if len(sys.argv) >= 4 else 0.1
# INP_FILE   = 'input.txt'
CHUNK_SIZE = 512

server_address = (__HOST, __PORT)

connection = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
connection.bind(server_address)

seq_num      = 0
pkts_sent    = 0
pkts_re_sent = 0
last_pkt     = None

to_drop      = False

pckts_timer  = None

def close_server(signal, frame):
	print "\b\bClosing..."
	connection.close()
	exit(0)

def handle_timeout(header_dict):
	print '\nTIMEOUT\n'
	pckts_timer.cancel()
	# print 'resending'
	connection.sendto(last_pkt, client_address)

	header, data = packet.unpack(last_pkt, False)
	packet.details(header_dict, data, 'RTRS', True)


	global pkts_re_sent
	pkts_re_sent += 1

	# print 'restart timer'
	global pckts_timer
	pckts_timer = Timer(0.5, handle_timeout, (header_dict, ))
	pckts_timer.start()


def should_i_send():
	global packets_should_be_sent
	global packets_should_be_lost

	if packets_should_be_sent:
		toSentOrNotToSent = randint(0, 1)
		if(packets_should_be_lost == 0):
			#send all directly
			packets_should_be_sent -= 1
			return True
	 	elif(packets_should_be_sent == packets_should_be_lost):
	 		#all remaining packets will be lost and retransmitted
	 		packets_should_be_sent -= 1 
	 		packets_should_be_lost -= 1
	 		return False
	 	else:
			if(toSentOrNotToSent == 1):
				#send
				packets_should_be_sent-=1
				return True

			else:
				#packet is lost :O
				packets_should_be_lost -= 1
				return False


signal.signal(signal.SIGINT, close_server)

# input_file = open(INP_FILE, 'r')


print 'connected...'

while True:
	msg, address = connection.recvfrom(1024)
	client_address  = address

	if pckts_timer != None:
		# print 'stopping timer'
		pckts_timer.cancel()


	# print 'raw request from client: \n', msg

	header, data = packet.unpack(msg, False)
	header_dict = packet.dictionarify(header);

	# Check if packet is valid
	if not packet.isValid(msg): 
		# Handle error in checksum (Send last packet again)	
		connection.sendto(last_pkt, client_address)

		pckts_timer = Timer(0.5, handle_timeout, (header_dict, ))
		pckts_timer.start()

		continue
	else:
		packet.details(header, data, 'RECV')

	# If sender received ack for different sequance number
	# Retransmit the last packet
	if header_dict['seq_num'] != seq_num and not header_dict['FIN']:
		# Handle error in checksum (Send last packet again)	
		connection.sendto(last_pkt, client_address)

		pckts_timer = Timer(0.5, handle_timeout, (header_dict, ))
		pckts_timer.start()

		continue

		

	# For Test drop packet; skip it
	# if header_dict['ACK'] and header_dict['seq_num'] == 1 and to_drop:
	# 	global to_drop
	# 	to_drop = False
	# 	continue

	# In case of final ack
	if header_dict['FIN'] and not header_dict['ACK']:
		seq_num = header_dict['seq_num']

		# Receiver want to finsh
		# sender respond with ack
		ack_pkt = packet.create(__PORT, header_dict['src_port'], seq_num, packet.set_flags(FIN=True, ACK=True))
		connection.sendto(ack_pkt, client_address)

		seq_num += 1
		pkts_sent += 1


		fin_pkt = packet.create(__PORT, header_dict['src_port'], seq_num, packet.set_flags(FIN=True, ACK=False))
		connection.sendto(fin_pkt, client_address)

		seq_num += 1
		pkts_sent += 1
		last_pkt = fin_pkt

		pckts_timer = Timer(0.5, handle_timeout, (header_dict, ))
		pckts_timer.start()

		# Galna ACK from receiver
		msg, address = connection.recvfrom(1024)
		client_address = address
		pckts_timer.cancel()

		header, data = packet.unpack(msg)
		header_dict = packet.dictionarify(header);

		break


	# In case of SYN send back ACK
	if header_dict['SYN']:
		INP_FILE = data
		# print 'input', INP_FILE
		input_file = open(INP_FILE, 'r')

		file_size = os.path.getsize(INP_FILE)
		packets_should_be_sent = math.ceil(float(file_size) / float(CHUNK_SIZE))   #number of packets


		packets_should_be_lost = int(packets_should_be_sent * lossProbability)
		print 'LOSS PROBABILITY: %f, packets_should_be_lost %d, packets_should_be_sent %d' % (lossProbability, packets_should_be_lost, packets_should_be_sent)
		raw_input('press enter to continue')

		ack_packet = packet.create(__PORT, header_dict['src_port'], seq_num, packet.set_flags(SYN=True, ACK=True))
		connection.sendto(ack_packet, client_address)



	# In case of NACK
	if seq_num != 0 and header_dict['seq_num'] == seq_num and not header_dict['ACK']:
		# retransmit
		# print 'Packet #1 dropped'
		packet.details(header, data, "RTRS")
		connection.sendto(last_pkt, client_address)
		pkts_re_sent += 1

		continue

	seq_num += 1

	# Go to the point we want in file
	input_file.seek(seq_num * CHUNK_SIZE - CHUNK_SIZE, 0)
	data = input_file.read(CHUNK_SIZE) # just at first read 500 bytes by 500 bytes

	if data == '':
		flags = packet.set_flags(ACK=False)
		data = 'EOP'
	else:
		flags = packet.set_flags(ACK=False)
		# data = 'packet #%d' % seq_num
	

		

	if data == 'EOP' or (header_dict['SYN'] and seq_num != 1):
		# print '(EOP): SEQ NUM %d' % seq_num
		new_pckt = packet.create(__PORT, header_dict['src_port'], seq_num, flags, data)
		connection.sendto(new_pckt, client_address)
	elif should_i_send():
		# print '(SHOULD I SEND): SEQ NUM %d' % seq_num
		new_pckt = packet.create(__PORT, header_dict['src_port'], seq_num, flags, data)
		connection.sendto(new_pckt, client_address)
	else:
		# print '(ZIZO): SEQ NUM %d' % seq_num
		# x = seq_num+1
		header_dict['seq_num'] = seq_num
		new_pckt = packet.create(__PORT, header_dict['src_port'], seq_num, flags, data, False)
		packet.details(header_dict, data, 'ZIZO', True)


	pkts_sent += 1
	last_pkt = new_pckt

	# print 'Start base timer'
	pckts_timer = Timer(0.5, handle_timeout, (header_dict, ))
	pckts_timer.start()





print 'Closing ...'
connection.close()