#The awesome reilable transport layer
---

## Requirements:

- Ubuntu or any *NIX based operating system.
- Python 2.7.*
- Any Input file to be able to send it from sender to receiver



## How to run:
	
1. `cd` into *SR* or *Stop and Wait* directory
2. open 2 terminals 1 for sender & the other one for receiver
3. In The sender terminal boot up server by typing 

	`python sender.py localhost 6666` 

	(you can replace localhost and 6666 with any ip or port you want)

4. In The receiver terminal boot up server by typing 

	`python receiver.py localhost 6666 input.txt output.txt` 

	(you can replace localhost and 6666 with any ip or port you want, you can change input.txt & output.txt to any file in the same directory)

5. Enjoy !



**Ramy Tamer 				2162**
**Ahmad Abdel-Aziz 		2210**
**Amir Said Abdeldayem 	2457**
