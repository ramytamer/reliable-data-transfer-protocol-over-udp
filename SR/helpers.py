import struct
import time
import signal

# SET BY OTHER FILES
connection = None

HEADER_FORMAT = '!HHII???I'
HEADER_LENGTH = 19

# for test purpose
to_drop = True

def calc_checksum(pkt):
	if(len(pkt) % 2 == 1):
		pkt = pkt + "0"

	summ = 0
	for i in range(0, len(pkt) , 2):
		if i+1 < len(pkt):
			word = ord(pkt[i]) + (ord(pkt[i+1]) << 8)
			interSum = summ + word
			summ = (interSum & 0xffff) + (interSum >> 16)	

	return ~summ & 0xffff

def isValid(pkt):
	header_without_checksum = pkt[ : HEADER_LENGTH - 4 ] + 4*chr(0)
	checksum = struct.unpack('!I', pkt[ HEADER_LENGTH - 4 : HEADER_LENGTH ])[0]
	data = pkt[ HEADER_LENGTH :  ]

	# Some noise for testing
	# if data == 'packet #1' and to_drop:
	# 	print "dropping packet #1"
	# 	header_without_checksum = header_without_checksum + 'some noise here'
	# 	global to_drop
	# 	to_drop = False

	recved_checksum = calc_checksum(header_without_checksum + data)
	# print 'checksum: %s == %s' % (str(recved_checksum), str(checksum))
	return recved_checksum == checksum


def create(src_port, dest_port, seq_num, flags, data='', with_details=True, address=False):

	header = struct.pack(
						HEADER_FORMAT, src_port,
						dest_port, seq_num,
						HEADER_LENGTH, flags['SYN'],
						flags['FIN'], flags['ACK'], 0) # 0 -> checksum

	checksum = calc_checksum(header + data)

	header = struct.pack(
						HEADER_FORMAT, src_port,
						dest_port, seq_num,
						HEADER_LENGTH, flags['SYN'],
						flags['FIN'], flags['ACK'], checksum)

	if with_details:
		details(struct.unpack(HEADER_FORMAT, header), data, 'SEND', address=address)

	return header + data


def unpack(pckt, with_details=True, address=False):
	header = pckt[:HEADER_LENGTH]
	header = struct.unpack(HEADER_FORMAT, header)

	data = pckt[HEADER_LENGTH:] 

	if with_details:
		details(header, data, address=address)

	return header, data


def details(header, data=False, status='RECV', is_dict=False, address=False):
	if not is_dict:
		header = dictionarify(header)


	if header['SYN'] or header['FIN']:
		print '==================PACKET (%s)===========================' % status
		print "from: %s to: %s, seq#: %s" % (header['src_port'], header['dest_port'], header['seq_num'])
		print "HL: %s, SYN: %s, FIN: %s, ACK: %s, Checksum: %s" % ( header['HL'], header['SYN'], header['FIN'], header['ACK'], header['checksum'])
		print '=====================EOP==================================\n'

	else:

		print "(%-5s) #%-6d [%-5s][%-5s][%-5s][%-5d]" % (status, header['seq_num'], header['SYN'], header['FIN'], header['ACK'], header['checksum'])


	'''
	print '==================PACKET (%s)===========================' % status
	print "from: %s to: %s, seq#: %s" % (header['src_port'], header['dest_port'], header['seq_num'])
	print "HL: %s, SYN: %s, FIN: %s, ACK: %s, Checksum: %s" % ( header['HL'], header['SYN'], header['FIN'], header['ACK'], header['checksum'])

	if data:
		print "data: (%s)" %  data

	print '=====================EOP==================================\n'
	# time.sleep(1.5)
	# raw_input('press enter to continue')
	'''

def dictionarify(header_tuple):
	header = {
		'src_port' : header_tuple[0],
		'dest_port': header_tuple[1],
		'seq_num'  : header_tuple[2],
		'HL'       : header_tuple[3],
		'SYN'      : header_tuple[4],
		'FIN'      : header_tuple[5],
		'ACK'      : header_tuple[6],
		'checksum' : header_tuple[7],
	}

	return header


def set_flags(SYN=False, FIN=False, ACK=True):
	return {
		'SYN': SYN,
		'FIN': FIN,
		'ACK': ACK
	}


def close_server(signal, frame):
	print "\b\bClosing (ctrl+c)..."
	connection.close()
	exit(0)