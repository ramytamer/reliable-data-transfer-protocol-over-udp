from Packet import *
import socket
import os
import signal
import sys
import helpers
import timeit

start = timeit.default_timer()

os.system('clear')

# Constants
__HOST      = sys.argv[1]
__PORT      = int(sys.argv[2])
INP_FILE    = str(sys.argv[3]) if len(sys.argv) >= 4 else 'input.txt'
OUT_FILE    = str(sys.argv[4]) if len(sys.argv) >= 5 else 'output'

WINDOW_SIZE = 5
CHUNK_SIZE  = 512 # 512 bytes at packet

bfr = {}
SOW = 1

server_address = (__HOST, __PORT)
connection = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# connection.bind(('localhost', 0))

helpers.connection = connection

print 'Connecting...'


# BOOTSTRAP OUTPUT FILE AREA
if os.path.isfile(OUT_FILE) :
	os.remove(OUT_FILE)
	# Empty the file
	# open(OUT_FILE, 'w').close()

# Open file in append mode
output_file = open(OUT_FILE, 'a')

# Listen to Ctrl+c
signal.signal(signal.SIGINT, helpers.close_server)

# Globals
src_port  = connection.getsockname()[1]
packets   = {}
seq_num   = 0 	# Current sequance number
pkts_sent = 0	# Number of packets have been sent

try:
	# Send SYN packet (handshake)
	handshake_pkt = helpers.create(src_port, __PORT, seq_num, helpers.set_flags(SYN=True, ACK=False), INP_FILE, address=server_address)
	connection.sendto(handshake_pkt, server_address)

	# Begin to receive packets
	while True:
		msg, address = connection.recvfrom(helpers.HEADER_LENGTH + CHUNK_SIZE)

		server_address = address

		header, data = helpers.unpack(msg, False)
		header_dict = helpers.dictionarify(header)

		# print 'i received %s' % data

		# If packet is not valid
		if not helpers.isValid(msg):
			# just ignore it
			# print 'corrupted !'
			helpers.details(header, data, 'CORR', address=server_address)
			continue
		else:
			helpers.details(header, data, 'RECV', address=server_address)

		# Sender got our SYN and ACKing it
		if header_dict['SYN'] and header_dict['ACK']:
			continue
		
		# If last packet Area
		if header_dict['FIN']:
			if header_dict['ACK']:
				continue
			else:
				output_file.close()
				# header_dict['seq_num'] += 1
				flags = helpers.set_flags(FIN=True, ACK=True)
				connection.settimeout(0.5)
		else:

			# Check if duplicate (acked) packet came from sender
			if header_dict['seq_num'] in packets :
				# Send ACK again
				pkt = packets[ header_dict['seq_num'] ]

				new_pkt = helpers.create(src_port, __PORT, header_dict['seq_num'], helpers.set_flags(), pkt.data, False, server_address)
				new_pkt = Packet(new_pkt)

				packets[ header_dict['seq_num'] ]  = new_pkt
				packets[ header_dict['seq_num'] ].send(connection, server_address, 'RACK')
				packets[ header_dict['seq_num'] ].start_timer(connection, server_address)

				continue


			if data == 'EOP':
				# END OF PACKETS
				# print 'i am sending EOP'
				flags = helpers.set_flags(FIN=True, ACK=False)
				header_dict['seq_num'] += 1
			else:
				sqnum = header_dict['seq_num']
				pkt = Packet(msg)
				pkt.mark_as_acked()

				# print 'keys',
				# for k in bfr.iterkeys():
				# 	print k, ',', 
				# print '\n'

				if  sqnum == SOW :
					# first write into file this packet
					# print 'writing #', sqnum
					output_file.write(pkt.data)

					# Write all remaining packets
					i = SOW + 1
					while i in bfr:
						# print 'Writing #', i
						output_file.write( bfr[ i ].data )
						# Remove from buffer (update window)
						bfr.pop(i, None)

						# Update Start of window
						i += 1

					SOW = i
					# print 'SOW is now', SOW

				else:
					bfr[ sqnum ] = pkt

				flags = helpers.set_flags()
				# add ACKed packet to hashtable
				packets[ header_dict['seq_num'] ] = pkt


		# Send ACK to server
		ack_packet = helpers.create(src_port, __PORT, header_dict['seq_num'], flags, address=server_address)
		connection.sendto(ack_packet, server_address)






	

except socket.timeout:
	print 'Closing...'
	connection.close()

connection.close()



stop = timeit.default_timer()

print 'Elapsed Time: ', stop - start 