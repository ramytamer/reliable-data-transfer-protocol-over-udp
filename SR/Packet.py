import helpers
from threading import Timer
import socket

class Packet():
	def __init__(self, packed_pkt):
		self.packed_pkt = packed_pkt
		header, data = helpers.unpack(packed_pkt, False)

		self.header = header
		self.data = data
		self.acked = False
		self.timer = None

	def send(self, connection, address, status='SEND'):
		connection.sendto(self.packed_pkt, address)
		# seq_num = helpers.dictionarify(self.header)['seq_num']

		helpers.details(self.header, self.data, status, address=address)
		

	def start_timer(self, connection, address):
		self.timer = Timer(0.05, self.handle_timeout, (connection, address))
		self.timer.start()

	def handle_timeout(self, connection, address):
		seq_num = helpers.dictionarify(self.header)['seq_num']
		print '\nTIMEOUT\n'
		if self.timer != None and self.timer.isAlive():
			self.timer.cancel()

		if not self.acked:
			self.send(connection, address, 'RTRS')

	def mark_as_acked(self):
		seq_num = helpers.dictionarify(self.header)['seq_num']
		if self.timer != None and self.timer.isAlive():
			# print 'stoping timer for %d' % seq_num
			self.timer.cancel()

		self.acked = True




